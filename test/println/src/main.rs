#[no_std];
#[allow(ctypes, cstack)];

pub enum c_void {}

extern {
	pub fn print() -> c_void;
}


#[no_mangle]
fn main()
{
	unsafe {
		print();
	}
}
