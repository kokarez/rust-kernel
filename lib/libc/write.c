static int pos = 0;

int write(int fd, char *buf, int len)
{
	char *fb;
	while (len--)
	{
		fb = ((char*)0xb8000) + pos;
		*fb = *buf++;
		pos += 2;
	}
	return len;
}
