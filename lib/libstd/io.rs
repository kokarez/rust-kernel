#[no_std]

use libc::types::common::c95::c_char;

extern {
	pub fn strcpy(dst: *c_char, src: *c_char) -> *c_char;
	pub fn write(fd: int, buf: *mut u8, len: int) -> int;
}

extern "rust-intrinsic" {
	pub fn transmute<T, U>(e: T) -> U;
}

#[lang = "free"]
pub fn free() {

}

#[lang = "exchange_free"]
unsafe fn exchange_free(_ptr: *mut u8) {
}

#[no_mangle]
pub unsafe fn print_char(c: char) {
	#[fixed_stack_segment];
	//write(0, (c as u8), 1);
}

#[no_mangle]
pub fn print(s1: &str) {
	#[fixed_stack_segment];

	unsafe {
		let sbuf: *mut u8 = transmute(&s1);
		write(0, sbuf, 6);
	}
}
