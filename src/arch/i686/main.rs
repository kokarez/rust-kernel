#[allow(ctypes, cstack)];
#[no_std];
#[no_core];

extern  {
	pub fn print_char(fake: int, c1: char);
	pub fn print(fake: int,  c1: &str);
}

#[no_mangle]
pub unsafe fn main()
{
	print(0, &"Hello2");
	while (true) {};
}
