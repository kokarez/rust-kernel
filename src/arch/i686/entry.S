#define MAGIC 0x1BADB002
#define FLAGS 0x00000003 /* PAGE_ALIGN | MEMORY_INFO */

.section .multiboot
  .align  4
  .long   MAGIC
  .long   FLAGS
  .long   -(MAGIC + FLAGS)

.section .text
.global entry
entry:
  movl $0, %gs:0x30
  mov $_stack_end, %esp
  xor %ebp, %ebp
  push %ebx
  push %eax
  call main

.global __morestack
__morestack:

	jmp __morestack
