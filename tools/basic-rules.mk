# verbose ?
ifeq ($(VERBOSE), 1)
	MAKEFLAGS       =
else
	MAKEFLAGS       = --silent --no-print-directory
endif

%.o: %.rs
	@echo -e "[BRAIN-FUCK-BEGIN]\t$<"
	@echo -e "[RUSTC-TO-ASM]\t\t$< --> $(@:%.o=%.s)"
	$(RUSTC) --target i686-pc-linux -S --lib $<
	@echo -e "[SED-CLEAN]"
	cat $(@:%.o=%.s) | sed "/abi_version/d" > $(@:%.o=%.s)2
	cp $(@:%.o=%.s)2 $(@:%.o=%.s)
	@echo -e "[CC-TO-RELOC]\t$(@:%.o=%.s) --> $@"
	$(CC) $(CFLAGS) -c -o $@ $(@:%.o=%.s)
	@echo -e "[CLEAN]"
	rm $(@:%.o=%.s)
	rm $(@:%.o=%.s)2
	@echo -e "[BRAIN-FUCK-END]\t$@"

%.bc: %.rs
	@echo "[BC-RUSTC] $< --> $@"
	$(RUSTC) --emit-llvm --lib $<
	@echo "[LLVM-EXTRACT] $@"
	llvm-extract --rfunc ".*" --rglob ".*a[^b][^i].*" $@ -o $@

%.o: %.S
	@echo "[ASM] $< --> $@"
	$(CC) $(CFLAGS) -c -o $@ $^

%.o: %.c
	@echo "[CC] $< --> $@"
	$(COMPILE.c) $(OUTPUT_OPTION) $<

%.bc: %.c
	@echo "[BC-CC] $< --> $@"
	$(COMPILE.c) $(OUTPUT_OPTION) $<
