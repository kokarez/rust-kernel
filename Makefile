sinclude config.mk
sinclude tools/basic-rules.mk

TARGET	=rust_kernel

SRCRS		= src/arch/i686/main.rs
SRCASM	= src/arch/i686/entry.S
OBJ			= $(SRCRS:%.rs=%.o) $(SRCASM:%.S=%.o)
LIBS		= lib/lib.a
CFLAGS 	= -g -m32 #-L/usr/local/lib/rustc/$(FOLDER_LIB)/lib -g -m32
LDFLAGS	= -nostdlib -m32

all: $(TARGET)

floppy: $(TARGET)
	cp tools/floppy rust_kernel.floppy
	mcopy -i rust_kernel.floppy rust_kernel ::/modules/ipos

%.a:
	$(MAKE) -C lib `basename $@`

boot: floppy
	qemu-system-i386 --no-kvm -fda rust_kernel.floppy

debug: floppy
	qemu-system-i386 --no-kvm -fda rust_kernel.floppy -s -S&
	sleep 1
	gdb rust_kernel

$(TARGET): $(OBJ) $(LIBS)
	@echo -e "[LD] $@"
	gcc $(CFLAGS) -T src/arch/i686/link.ld $(LDFLAGS) -o $@ $^

%.bc:
	$(MAKE) -C lib `basename $@`

clean:
	$(MAKE) -C lib clean
	rm -rf $(OBJ) $(TARGET)

distclean: clean
#	$(MAKE) -C lib distclean
	rm -rf config.mk rust_kernel.floppy rust_kernel

config.mk:
	./configure
